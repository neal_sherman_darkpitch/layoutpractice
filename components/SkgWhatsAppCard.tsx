import * as React from 'react';
import { StyleSheet, Text } from 'react-native';
import { Card } from 'react-native-paper';
import { SkgColumn } from './SkgColumn';

import { SkgRow } from './SkgRow';

export const SkgWhatsAppCard: React.FC<React.HTMLProps<any>> = (props) => {
  const styles = StyleSheet.create({
    containerCard: {
      ...props.style,
      height: 72
    },
    heading: {
      fontWeight: '600'
    }
  });

  return (
    <Card style={styles.containerCard}>
      <SkgRow vCenter style={{ height: '100%', padding: 10 }} gap={10}>
        <Text>ICON</Text>
        <SkgColumn style={{ flex: 1 }}>
          <Text style={styles.heading}>Chat with us on WhatsApp!</Text>
          <Text>Click to get started on your weight loss journey</Text>
        </SkgColumn>
      </SkgRow>
    </Card>
  );
};
