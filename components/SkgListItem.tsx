import * as React from 'react';
import { StyleSheet, StyleProp } from 'react-native';
import { Text } from 'react-native-paper';

import { SkgColumn } from './SkgColumn';
import { SkgRow } from './SkgRow';

interface SkgListItemProps extends React.HTMLProps<any> {
  item: { name: string; value: any };
  fontStyle: StyleProp<any>;
}

export const SkgListItem: React.FC<SkgListItemProps> = (props) => {
  return (
    <SkgColumn
      vBottom
      style={{ ...props.style, borderBottom: 1, borderColor: 'red', borderStyle: 'solid' }}
    >
      <SkgRow className='textGroup' vCenter style={{ padding: 6 }} hSpaceBetween>
        <Text style={props.fontStyle}>{props.item.name}</Text>
        <Text style={props.fontStyle}>{props.item.value}</Text>
      </SkgRow>
    </SkgColumn>
  );
};
