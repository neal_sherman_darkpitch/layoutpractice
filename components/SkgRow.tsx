import * as React from 'react';
import { StyleSheet, View } from 'react-native';

interface SkgRowProps extends React.HTMLProps<any> {
  /** Vertically center the content in the container. By default, content is vertically aligned
   to the top of the container. */
  vCenter?: boolean;
  /** Vertically align the content to the bottom of the container. By default, content is
    vertically aligned to the top of the container. */
  vBottom?: boolean;
  /** Horizontally center the content of each child. By default, content is aligned left. */
  hCenter?: boolean;
  /** Horizontally align the content of each child to the right. By default, content is aligned
   left. */
  hRight?: boolean;
  /** If set, horizontally space the children evenly across the entire container, with the first and
   last children positioned against the edges of the container. */
  hSpaceBetween?: boolean;
  /** If set, horizontally space the children evenly across the entire container, including spacing
   between the edges of the container and the first and last children. */
  hSpaceAround?: boolean;
  /** How many pixels to render between each child. */
  gap?: number;
  /** For debugging purposes. Render borders around containing View (purple) and each child (cyan). */
  showBorders?: boolean;
}

/**
 *
 * This component provides a `View` container that lays out its children in a flex row. Various
 * props can be passed to this component to affect the positioning and layout of its children (see
 * the props interface for more details).
 *
 */
export const SkgRow: React.FC<SkgRowProps> = (props) => {
  // We start with display: 'none' then set it to 'flex' in the useEffect hook to prevent things
  // from shifting around as the page loads initially.
  const [contentStyles, setContentStyles] = React.useState<any>({
    ...props.style,
    display: 'none',
    flexDirection: 'row',
    borderColor: props.showBorders ? 'purple' : null,
    borderWidth: props.showBorders ? 1 : null
  });

  const childStyles: any = {
    marginRight: props.gap || 0,
    borderColor: props.showBorders ? 'cyan' : null,
    borderWidth: props.showBorders ? 2 : null
  };

  const [lastChildIndex, setLastChildIndex] = React.useState<number>(
    React.Children.count(props.children)
  );

  const isLastChild = (index: number): boolean => index === lastChildIndex - 1;

  React.useEffect(() => {
    let configuredStyles: any = {};
    if (props.vCenter) {
      configuredStyles.alignItems = 'center';
    } else if (props.vBottom) {
      configuredStyles.alignItems = 'flex-end';
    }

    if (props.hCenter) {
      configuredStyles.justifyContent = 'center';
    } else if (props.hRight) {
      configuredStyles.justifyContent = 'flex-end';
    } else if (props.hSpaceBetween) {
      configuredStyles.justifyContent = 'space-between';
    } else if (props.hSpaceAround) {
      configuredStyles.justifyContent = 'space-around';
    }

    setContentStyles({ ...contentStyles, ...configuredStyles, display: 'flex' });

    // We need to treat the last child differently as we don't want a margin behind it. This index
    // and function help us identify the last child.
    setLastChildIndex(React.Children.count(props.children));
  }, []);

  return (
    <View style={contentStyles}>
      {React.Children.map(props.children, (child, i) => {
        if (isEl(child)) {
          // The `style` props attached to the children may be either an object (eg.
          // {fontWeight: '200'}), or a number that was produced by StyleSheet.create(). So we
          // need to run `flatten` on the style to ensure the style is in object format.
          let mergedStyles = { ...StyleSheet.flatten(child.props.style), ...childStyles };
          mergedStyles = isLastChild(i) ? { ...mergedStyles, marginRight: 0 } : mergedStyles;

          return React.cloneElement(child, {
            style: mergedStyles
          });
        }
      })}
    </View>
  );
};

/** Helper type guard to make sure a child passed to this component is a React element. */
function isEl(x: any): x is React.ReactElement {
  return 'props' in x;
}
