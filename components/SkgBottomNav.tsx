import * as React from 'react';
import { BottomNavigation, Text } from 'react-native-paper';
import SkgMusic from '../screens/SkgMusic';
import SkgAlbums from '../screens/SkgAlbums';

const RecentsRoute = () => <Text>Recents</Text>;

const SkgBottomNav = () => {
  const [index, setIndex] = React.useState(0);
  const [routes] = React.useState([
    { key: 'music', title: 'Music', icon: 'home' },
    { key: 'albums', title: 'Albums', icon: 'album' },
    { key: 'recents', title: 'Recents', icon: 'history' }
  ]);

  const renderScene = BottomNavigation.SceneMap({
    music: SkgAlbums,
    albums: SkgMusic,
    recents: RecentsRoute
  });

  return (
    <BottomNavigation
      navigationState={{ index, routes }}
      onIndexChange={setIndex}
      renderScene={renderScene}
    />
  );
};

export default SkgBottomNav;
