import React from 'react';
import { Image, Pressable, StyleSheet, View } from 'react-native';
import { Button, Switch, Text } from 'react-native-paper';
import Icon from 'react-native-vector-icons/MaterialIcons';

import { SkgContainer } from '../components/SkgContainer';
import { SkgColumn } from '../components/SkgColumn';
import { SkgRow } from '../components/SkgRow';
import { SkgListItem } from '../components/SkgListItem';
import { SkgWhatsAppCard } from '../components/SkgWhatsAppCard';

export default function SkgAlbums() {
  const [isWaist, setIsWaist] = React.useState(true);

  return (
    <SkgContainer>
      <SkgColumn className='body' gap={24} style={{ paddingBottom: 30 }}>
        <Image
          source={require('../assets/blue_tagline_eng.png')}
          style={{ height: 174, width: 480, marginHorizontal: 'auto' }}
        />
        <SkgListItem
          item={{ name: 'Weight Loss to Date', value: '2kg' }}
          fontStyle={styles.listItem}
        ></SkgListItem>
        <SkgWhatsAppCard></SkgWhatsAppCard>
        <SkgRow className='metricSelection' gap={13} hSpaceBetween vCenter>
          <SkgRow className='metricToggle' gap={29} hCenter vCenter>
            <Text>Weight</Text>
            <Switch value={isWaist} onValueChange={() => setIsWaist(!isWaist)} />
            <Text>Waist</Text>
          </SkgRow>
          <Pressable onPress={() => {}}>
            <Icon name='tune' size={30} color='#900' />
          </Pressable>
        </SkgRow>
        <View style={{ height: 270, backgroundColor: 'grey' }}>
          <Text>Chart goes here</Text>
        </View>
        <Button mode='contained' onPress={() => {}}>
          Add Measurement
        </Button>
      </SkgColumn>
    </SkgContainer>
  );
}

const styles = StyleSheet.create({
  listItem: {
    fontSize: 24,
    fontWeight: '600'
  }
});
